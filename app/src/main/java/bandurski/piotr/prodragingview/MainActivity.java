package bandurski.piotr.prodragingview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import bandurski.piotr.prodragingview.mockers.Mocker;
import bandurski.piotr.prodragingview.ui.GridItemAdapter;
import bandurski.piotr.prodragingview.ui.SuperGridView;

public class MainActivity extends AppCompatActivity {

    SuperGridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridView = (SuperGridView) findViewById(R.id.gridview);
        GridItemAdapter gridItemAdapter=new GridItemAdapter(this,R.layout.grid_view_item, Mocker.getFakeGridViewItems(),gridView);
        gridView.setAdapter(gridItemAdapter);
    }


}
