package bandurski.piotr.prodragingview.mockers;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import bandurski.piotr.prodragingview.models.GridViewItem;
import bandurski.piotr.prodragingview.models.ListViewItem;

/**
 * Created by Piotr on 2016-07-07.
 */
public class Mocker {

    static Random generator = new Random();


    public static List<ListViewItem> getFakeListViewItems() {
        ArrayList<ListViewItem> listViewItems = new ArrayList<>();
        int random = generator.nextInt(2) + 3;
        for (int i = 0; i < random; i++) {

            listViewItems.add(new ListViewItem(getRandomString()));
        }
        return listViewItems;
    }

    public static ArrayList<GridViewItem> getFakeGridViewItems() {
        ArrayList<GridViewItem> gridViewItems = new ArrayList<>();
        int random = generator.nextInt(5) + 4;
        for (int i = 0; i < random; i++) {
            gridViewItems.add(new GridViewItem(getFakeListViewItems()));
        }
        return gridViewItems;
    }

    public static String getRandomString() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);

    }

}
