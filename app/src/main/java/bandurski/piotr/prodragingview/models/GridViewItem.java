package bandurski.piotr.prodragingview.models;

import java.util.List;

/**
 * Created by Piotr on 2016-07-07.
 */
public class GridViewItem {
    private List<ListViewItem> listViewItems;

    public GridViewItem(List<ListViewItem> listViewItems) {
        this.listViewItems = listViewItems;
    }

    public List<ListViewItem> getListViewItems() {
        return listViewItems;
    }

    public void setListViewItems(List<ListViewItem> listViewItems) {
        this.listViewItems = listViewItems;
    }

}
