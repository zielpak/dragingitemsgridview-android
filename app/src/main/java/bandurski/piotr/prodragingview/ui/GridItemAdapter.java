package bandurski.piotr.prodragingview.ui;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import bandurski.piotr.prodragingview.R;
import bandurski.piotr.prodragingview.models.GridViewItem;
import bandurski.piotr.prodragingview.ui.lists.CustomListView;
import bandurski.piotr.prodragingview.ui.lists.ListHolderAdapter;

/**
 * Created by Piotr on 2016-07-07.
 */
public class GridItemAdapter extends ArrayAdapter<GridViewItem> {
    Context context;
    int layoutResourceId;
    ArrayList<GridViewItem> data;
    GridView gridView;
    List<GridRowsHeightHandler> heightHandlers = new ArrayList<>();
    List<ListHolderAdapter> listHolderAdapters = new ArrayList<>();
    int columnNum = 3;//TODO change it

    public GridItemAdapter(Context context, int layoutResourceId, ArrayList<GridViewItem> data, GridView gridView) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.gridView = gridView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ListHolderAdapter holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ListHolderAdapter(context, row, parent, position/columnNum);
            holder.listView = (CustomListView) row.findViewById(R.id.listView);
            row.setTag(holder);
            listHolderAdapters.add(holder);
            listHolderAdapters.get(listHolderAdapters.size()-1).setRowHeightHandler(getSuitableHeightHandler());
            GridViewItem item = data.get(position);
            listHolderAdapters.get(listHolderAdapters.size()-1).setItems(item.getListViewItems());
        } else {
            holder = (ListHolderAdapter) row.getTag();
        }




        return row;
    }


    int actualHandlerid = -1;
    private GridRowsHeightHandler getSuitableHeightHandler() {
        int arrayIndex=listHolderAdapters.size()-1;
        int rowPosition = arrayIndex / columnNum;
        if ((rowPosition) > actualHandlerid) {
            actualHandlerid = rowPosition;
            GridRowsHeightHandler heightHandler = new GridRowsHeightHandler(actualHandlerid,gridView);
            heightHandlers.add(heightHandler);
        }
        heightHandlers.get(heightHandlers.size() - 1).addItem(listHolderAdapters.get(arrayIndex));
        return heightHandlers.get(heightHandlers.size() - 1);
    }


}

