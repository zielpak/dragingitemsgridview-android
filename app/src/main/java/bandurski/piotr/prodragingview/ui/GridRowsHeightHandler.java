package bandurski.piotr.prodragingview.ui;

import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import bandurski.piotr.prodragingview.ui.lists.ListHolderAdapter;

/**
 * Created by Piotr on 2016-07-07.
 */
public class GridRowsHeightHandler {

    private List<ListHolderAdapter> itemsInRow = new ArrayList<>();
    int higestItemInRowHeight;
    int rowId;
    GridView gridView;

    public GridRowsHeightHandler(int rowId, GridView gridView) {
        this.rowId = rowId;
        this.gridView = gridView;
    }

    public void addItem(ListHolderAdapter item) {
        itemsInRow.add(item);
    }

    public Integer getRowId() {
        return rowId;
    }

    public void suggestNewHigestHeight(int height) {
        if (higestItemInRowHeight < height) {
            higestItemInRowHeight = height;
        }
        setRowHeight();
    }

    public void suggestResizeRow() {
        int higestHeight = 0;
        for (ListHolderAdapter adapter : itemsInRow) {
            int adapterHeight = adapter.getListViewHeight();
            if (adapterHeight > higestHeight) {
                higestHeight = adapterHeight;
            }
        }
        higestItemInRowHeight = higestHeight;
        setRowHeight();

    }

    private void setRowHeight() {
        for (ListHolderAdapter adapter : itemsInRow) {
            adapter.setHeight(higestItemInRowHeight);
        }
        gridView.requestLayout();
    }

}
