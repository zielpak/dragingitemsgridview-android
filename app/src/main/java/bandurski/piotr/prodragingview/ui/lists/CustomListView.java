package bandurski.piotr.prodragingview.ui.lists;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

/**
 * Created by Piotr on 2016-07-07.
 */
public class CustomListView extends ListView {

    ListHolderAdapter listHolderAdapter;

    public CustomListView(Context context) {
        super(context);
    }

    public CustomListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ListHolderAdapter getListHolderAdapter() {
        return listHolderAdapter;
    }

    public void setListHolderAdapter(ListHolderAdapter listHolderAdapter) {
        this.listHolderAdapter = listHolderAdapter;
    }

}
