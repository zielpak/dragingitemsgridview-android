package bandurski.piotr.prodragingview.ui.lists;

import android.content.Context;
import android.view.View;

import bandurski.piotr.prodragingview.models.ListViewItem;

/**
 * Created by Piotr on 2016-07-07.
 */
public class DragListItem extends View{
    public ListViewItem listViewItem;

    public DragListItem(Context context) {
        super(context);
    }
}
