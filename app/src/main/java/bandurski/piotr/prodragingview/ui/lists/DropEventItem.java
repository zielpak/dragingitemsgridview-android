package bandurski.piotr.prodragingview.ui.lists;

import android.view.ViewGroup;

import bandurski.piotr.prodragingview.models.ListViewItem;

/**
 * Created by Piotr on 2016-07-07.
 */
public class DropEventItem {
    ListHolderAdapter oldOwner;
    ListViewItem item;

    public DropEventItem(ListHolderAdapter oldOwner, ListViewItem item) {
        this.oldOwner = oldOwner;
        this.item = item;
    }

    public void removeItemFromOldOwner(){
        oldOwner.removeItem(item);
    }

    public ListHolderAdapter getOldOwner() {
        return oldOwner;
    }

    public void setOldOwner(ListHolderAdapter oldOwner) {
        this.oldOwner = oldOwner;
    }

    public ListViewItem getItem() {
        return item;
    }

    public void setItem(ListViewItem item) {
        this.item = item;
    }

}
