package bandurski.piotr.prodragingview.ui.lists;

import android.content.ClipData;
import android.content.Context;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;

import java.util.List;

import bandurski.piotr.prodragingview.R;
import bandurski.piotr.prodragingview.models.ListViewItem;
import bandurski.piotr.prodragingview.ui.GridRowsHeightHandler;

/**
 * Created by Piotr on 2016-07-07.
 */
public class ListHolderAdapter {
    Context context;
    public CustomListView listView;;
    List<ListViewItem> items;
    GridRowsHeightHandler rowsHeightHandler;
    MyListAdapter adapter;
    View parent;
    int rowId;

    public ListHolderAdapter(Context context, View view,View parent,int rowId) {
        this.context = context;
        this.parent=parent;
        this.rowId=rowId;
        listView = (CustomListView) view.findViewById(R.id.listView);
        listView.setListHolderAdapter(this);
        initListView();
    }

    public void setItems(List<ListViewItem> items){
        this.items=items;
        adapter=new MyListAdapter(context,items);
        listView.setAdapter(adapter);
        changeListViewHeight();
    }

    public View getParent() {
        return parent;
    }

    public void changeListViewHeight(){
            int height= getListViewHeight();
            setHeight(height);
    }

    public void setRowHeightHandler(GridRowsHeightHandler rowsHeightHandler){
        this.rowsHeightHandler=rowsHeightHandler;
    }

    public void setHeight(int height){
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public int getListViewHeight(){
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int numberOfItems = listAdapter.getCount();
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }
            int totalDividersHeight = listView.getDividerHeight() * (numberOfItems - 1);

            int finallHeight = totalItemsHeight + totalDividersHeight;
            return finallHeight;
        }else{
            return 0;
        }
    }

    public void addItem(ListViewItem item){
        adapter.add(item);
        adapter.notifyDataSetChanged();
        changeListViewHeight();
        rowsHeightHandler.suggestNewHigestHeight(getListViewHeight());
    }

    public void removeItem(ListViewItem item){
        adapter.remove(item);
        adapter.notifyDataSetChanged();
        changeListViewHeight();
        rowsHeightHandler.suggestResizeRow();
    }

    private void initListView(){
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View view, int pos, long id) {
                DropEventItem item = new DropEventItem(ListHolderAdapter.this,items.get(pos));
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, item, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            }
        });
        listView.setOnDragListener(new MyDragListener());
    }

}

class MyDragListener implements View.OnDragListener {

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                // do nothing
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                // v.setBackgroundDrawable(enterShape);
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                //  v.setBackgroundDrawable(normalShape);
                break;
            case DragEvent.ACTION_DROP:
                DropEventItem item = (DropEventItem) event.getLocalState();
                item.removeItemFromOldOwner();
                CustomListView customListView=(CustomListView) v;
                customListView.getListHolderAdapter().addItem(item.getItem());
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                System.out.print("dd");
            default:
                break;
        }
        return true;
    }
}
