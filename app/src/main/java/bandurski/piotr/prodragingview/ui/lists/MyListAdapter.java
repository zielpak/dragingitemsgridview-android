package bandurski.piotr.prodragingview.ui.lists;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import bandurski.piotr.prodragingview.R;
import bandurski.piotr.prodragingview.models.ListViewItem;
import bandurski.piotr.prodragingview.ui.GridItemAdapter;

/**
 * Created by Piotr on 2016-07-07.
 */
public class MyListAdapter extends ArrayAdapter<ListViewItem> {

    private final Context context;
    ListViewItem item;
    TextView textView;

    public MyListAdapter(Context context, List<ListViewItem> listViewItemArrayAdapter) {
        super(context,-1,listViewItemArrayAdapter);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        item = getItem(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_view_item, parent, false);
        textView = (TextView) rowView.findViewById(R.id.text);
        textView.setText(item.text);
        return rowView;
    }


}
